# Upstreet

# Project Description: Know Your Customer (KYC) checks to identify new customers

# Steps and assumptions

# Download the project and run npm install.
# If you dont have typescript install then you have to install it.
# Run this command $ npm start


API  Details

    URL: localhost:4000/verify-licence
    
    Method: POST
    
    Request:  
        birthDate:1985-02-08
        givenName:James
        familyName:Smith
        middleName:Robert
        licenceNumber:94977000
        stateOfIssue:NSW
        expiryDate:2020-01-01
    
    Response:
        {
            "status": "success",
            "message": "driver licence verified",
            "data": {
                "verificationDocumentResult": {
                    "type": "DriverLicenceResponse"
                },
                "verificationRequestNumber": 85975,
                "verificationResultCode": "Y"
            }
        }
    


# Thanks for Reading
