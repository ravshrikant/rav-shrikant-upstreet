import { Application, Request, Response } from 'express';
import  { upStreetController } from "../controllers/upStreetController"; 
export class UpstreetRoutes {
    private upStreetController : upStreetController = new upStreetController();
    public route(app: Application) {
        app.post('/verify-licence',(req: Request, res: Response) =>{
            this.upStreetController.verifyDriverlicence(req,res)
        });
        
        // Mismatch URL
        app.all('*', function (req: Request, res: Response) {
            res.status(404).send({ error: true, message: 'Check your URL please' });
        });

    }
}
