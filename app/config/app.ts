import * as express from "express";
import * as bodyParser from "body-parser";
import * as mongoose from 'mongoose';
import environment from "../environment";
import { Middlewares } from "../middlewares/middlewares";
import { UpstreetRoutes } from "../routes/common_routes";
class App {

   public app: express.Application;
   public mongoUrl: string = 'mongodb://localhost/' + environment.getDBName();
   
   private middlewares:Middlewares = new Middlewares();
   /* private database: mongoose.Connection; */
   private upstreet_routes: UpstreetRoutes = new UpstreetRoutes();
   constructor() {
      this.app = express();
      this.app.use(this.middlewares.corsAccess);
      this.config();
     /*  this.mongoSetup(); */
      this.upstreet_routes.route(this.app);
   }

   private config(): void {
      // support application/json type post data
      this.app.use(bodyParser.json());
      //support application/x-www-form-urlencoded post data
      this.app.use(bodyParser.urlencoded({ extended: false }));
   }
/* 
   private mongoSetup(): void {
      mongoose.connect(this.mongoUrl, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: true });
      this.database = mongoose.connection;
      this.database.once("open", async () => {
         console.log("Connected to database");
      });
      this.database.on("error", (err) => {
         console.log(`Error connecting to database ${err}`);
      });
   } */

}
export default new App().app;