import { Request, Response } from 'express';
import * as request from "request-promise";
import { insufficientParameters, mongoError, successResponse, failureResponse } from '../modules/common/service';

export class upStreetController {


    public async verifyDriverlicence(req: Request, res: Response) {
        try {
            if(!req.body.birthDate){
                return res.status(400).json({ message: "birth date is required" });
            }
            if (!req.body.givenName) {
                return res.status(400).json({ message: "given name is required" });
            }
            if (!req.body.middleName) {
                return res.status(400).json({ message: "middle name is required" });
            }
            if (!req.body.familyName) {
                return res.status(400).json({ message: "family name is required" });
            }
            if (!req.body.licenceNumber) {
                return res.status(400).json({ message: "licence number is required" });
            }
            if (!req.body.stateOfIssue) {
                return res.status(400).json({ message: "state Of issue is required" });
            }
            if (!req.body.expiryDate) {
                return res.status(400).json({ message: "expiry date is required" });
            }
            let requestBody = { 
                "birthDate" : req.body.birthDate, 
                "givenName" : req.body.givenName, 
                "middleName" : req.body.middleName, 
                "familyName" : req.body.familyName, 
                "licenceNumber" : req.body.licenceNumber, 
                "stateOfIssue" : req.body.stateOfIssue, 
                "expiryDate" : req.body.expiryDate 
                };
            var options = {
                method: 'POST',
                uri: 'https://australia-southeast1-reporting-290bc.cloudfunctions.net/driverlicence',
                headers: {
                    Authorization: 'Bearer 03aa7ba718da920e0ea362c876505c6df32197940669c5b150711b03650a78cf'
                },
                body: requestBody,
                json: true // Automatically stringifies the body to JSON
            };
            request(options)
                .then(function (response) {
                    let message = '';
                    if(response.verificationResultCode === 'N'){
                        message = 'driver licence  not verified';
                    }else if(response.verificationResultCode === 'Y'){
                        message = 'driver licence verified';
                    }else if(response.verificationResultCode === 'D'){
                        message = "Document Error";
                    }else if(response.verificationResultCode === 'S'){
                        message = 'Server Error';
                    }
                    successResponse(message,response, res);
                    // POST succeeded...
                })
                .catch(function (err) {
                    failureResponse(err.message, null, res);
                });
          
        } catch (error: any) {
            failureResponse(error.message, null, res);
        }

    }
}