import { Request, Response , NextFunction } from 'express';

export class Middlewares {
    
    public corsAccess(req:Request,res:Response,next:NextFunction) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,Authorization");
        
        res.header("Access-Control-Allow-Methods", "POST,PUT,DELETE,GET"); 
        // intercept OPTIONS method
        if ('OPTIONS' === req.method) {
           return res.status(200);
        } else {
            next();
        }
    }
}

//export default Middlewares;