enum Environments {
    local_environment = 'local',
    dev_environment = 'dev',
    prod_environment = 'prod',
}

class Environment {
    private environment: String;

    constructor(environment: String) {
        this.environment = environment;
    }

    getPort(): Number {
        if (this.environment === Environments.prod_environment) {
            return 8081;
        } else if (this.environment === Environments.dev_environment) {
            return 8082;
        }else {
            return 4000;
        }
    }

    getDBName(): String {
        if (this.environment === Environments.prod_environment) {
            return 'prod_upstreet';
        } else if (this.environment === Environments.dev_environment) {
            return 'dev_upstreet';
        }else {
            return 'local_upstreet';
        }
    }
}

export default new Environment(Environments.local_environment);
