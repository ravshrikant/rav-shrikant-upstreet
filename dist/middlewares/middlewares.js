"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Middlewares = void 0;
class Middlewares {
    corsAccess(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,Authorization");
        res.header("Access-Control-Allow-Methods", "POST,PUT,DELETE,GET");
        // intercept OPTIONS method
        if ('OPTIONS' === req.method) {
            return res.status(200);
        }
        else {
            next();
        }
    }
}
exports.Middlewares = Middlewares;
//export default Middlewares;
