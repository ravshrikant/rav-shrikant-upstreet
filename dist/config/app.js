"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const bodyParser = require("body-parser");
const environment_1 = require("../environment");
const middlewares_1 = require("../middlewares/middlewares");
const common_routes_1 = require("../routes/common_routes");
class App {
    constructor() {
        this.mongoUrl = 'mongodb://localhost/' + environment_1.default.getDBName();
        this.middlewares = new middlewares_1.Middlewares();
        /* private database: mongoose.Connection; */
        this.upstreet_routes = new common_routes_1.UpstreetRoutes();
        this.app = express();
        this.app.use(this.middlewares.corsAccess);
        this.config();
        /*  this.mongoSetup(); */
        this.upstreet_routes.route(this.app);
    }
    config() {
        // support application/json type post data
        this.app.use(bodyParser.json());
        //support application/x-www-form-urlencoded post data
        this.app.use(bodyParser.urlencoded({ extended: false }));
    }
}
exports.default = new App().app;
