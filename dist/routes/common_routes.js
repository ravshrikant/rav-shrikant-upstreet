"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpstreetRoutes = void 0;
const upStreetController_1 = require("../controllers/upStreetController");
class UpstreetRoutes {
    constructor() {
        this.upStreetController = new upStreetController_1.upStreetController();
    }
    route(app) {
        app.post('/verify-licence', (req, res) => {
            this.upStreetController.verifyDriverlicence(req, res);
        });
        // Mismatch URL
        app.all('*', function (req, res) {
            res.status(404).send({ error: true, message: 'Check your URL please' });
        });
    }
}
exports.UpstreetRoutes = UpstreetRoutes;
